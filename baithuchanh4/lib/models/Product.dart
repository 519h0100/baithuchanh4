import 'package:flutter/material.dart';

class Product {
  final String image, title;
  final int price;
  final Color bgColor;

  Product({
    required this.image,
    required this.title,
    required this.price,
    this.bgColor = const Color(0xFFEFEFF2),
  });
}

List<Product> demo_product = [
  Product(
    image: "assets/images/product_0.png",
    title: "ADLV babyface 1",
    price: 50,
    bgColor: const Color(0xFFFEFBF9),
  ),
  Product(
    image: "assets/images/product_1.png",
    title: "MLB LA shirt",
    price: 99,
  ),
  Product(
    image: "assets/images/product_2.png",
    title: "ADLV babyface 2",
    price: 50,
    bgColor: const Color(0xFFF8FEFB),
  ),
  Product(
    image: "assets/images/product_3.png",
    title: "MLB New York Shirt",
    price: 99,
    bgColor: const Color(0xFFEEEEED),
  ),
  Product(
    image: "assets/images/product_3.png",
    title: "MLB New York Shirt for kids",
    price: 59,
    bgColor: const Color(0xFFEEEEED),
  ),

];
